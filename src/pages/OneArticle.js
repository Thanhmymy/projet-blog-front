import { Card, Col, Row } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "./OneArticle.css"


export function OneArticle() {
    const [article, setArticle] = useState();
    const [author, setAuthor] = useState();
    const { id } = useParams();

    useEffect(() => {

        async function fetchOne() {
            const response = await axios.get(process.env.REACT_APP_SERVER_URL +'/api/article/one/' + id);
            setArticle(response.data);

            const responseDeux = await axios.get(process.env.REACT_APP_SERVER_URL +'/api/user/one/' + response.data.user_id);
            setAuthor(responseDeux.data)
        }
        fetchOne();


    }, [id]);

    return (
        

        
        <Row  justify="center">
            <Col xs={24} md={12}>
            
            
            {article && author &&
                <div className="card">
                    <Card className="selector" title={author.pseudo} style={{ width: 800 }}><p className="condition">{article.texte}</p>
                    </Card>
                </div>

            }
        
            </Col>

        </Row>
        

    )
}