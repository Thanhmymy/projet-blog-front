import axios from "axios";
import { useEffect, useState } from "react"
import { useSelector } from "react-redux";
import { PersonalArticle } from "../component/Personal-article";
import { PersonalForm } from "../component/Personal-form";
import './personal.css'

export function Personal() {

    const [articles, setArticles] = useState([]);
    const user = useSelector(state => state.auth.user);



    async function addPost(article) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL + '/api/article', article);
        setArticles([...articles, response.data])
    }


    useEffect(() => {

        async function fetchArticleByUser() {
            const response = await axios.get(process.env.REACT_APP_SERVER_URL + '/api/article/user/' + user.id);
            setArticles(response.data)
        }
        if (user) {
            fetchArticleByUser();
        }

    }, [user])

    return (
        <div>
            
            <h1>My Space</h1>
            {user && <h1>Hello, {user.pseudo}</h1>}

            <div className="personal-article">
                <PersonalArticle articles={articles} />

            </div>
            <br /><br />
            <PersonalForm onFormSubmit={addPost} />


        </div>
    )
}