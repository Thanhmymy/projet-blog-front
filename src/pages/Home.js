
import { Col, Divider, Row } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { Article } from "../component/Article";
import './Home.css'

export default function Home() {

    const [article, setArticle] = useState([]);


    async function fetchArticle() {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL + '/api/article/all')
        setArticle(response.data)
    }

    useEffect(() => {
        fetchArticle();
    }, []);




    return (
        
            <div className="body">
            <br /><br />
                <Divider><h1>Home</h1></Divider>
                
                <Row>

                    {article.map(item => <Col key={item.id} xs={24} md={12} span={4}>
                        <Article  article={item} /></Col>)}

                </Row>
            </div>

        


    )
}

