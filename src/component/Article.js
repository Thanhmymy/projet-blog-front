import { Card, Col, Row } from "antd";
import { Link} from "react-router-dom";




import "./Article.css"


export function Article({ article }) {


    return (

            
            <Row justify="center">
                <Col xs={24} md={16}>
                <div className="card">
                <Link to= {'/article/one/' + article.id}>
                    
                    <Card className="selector" title="Article" cover={<img alt="example" src="https://resize-gulli.jnsmedia.fr/r/890,__ym__/img//var/jeunesse/storage/images/gulli/chaine-tv/dessins-animes/pokemon/pokemon/pikachu/26571681-1-fre-FR/Pikachu.jpg" />}><p className="condition">{article.texte}</p>
                    </Card>
                    <br /><br />
                    

                    </Link>
                
                </div>
                </Col>
                </Row>
        


            )
}


