import { Button, Form, Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../stores/auth-slice";
import './formulaire.css'


export function RegisterForm(){

    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.registerBlog);

    const onFinish = (values)=> {
        dispatch(register(values));
    }

    return(
        <>
        <Form
        name="basic"
        labelCol={{
            span: 8,
        }}
        wrapperCol={{
            span: 10,
        }}
        onFinish={onFinish}
    >
        {feedback && <p>{feedback}</p>}
        <Form.Item
            label="Email"
            name="email"
            rules={[
                {
                    required: true,
                    message: 'Email is required',
                },
                {
                    type: 'email',
                    message: 'Please enter a valid email',
                },
            ]}
        >
            <Input type="email" />
        </Form.Item>
            <Form.Item
            label="Pseudo"
            name = "pseudo"
            rules = {[
                {
                    required : true,
                    message :'Pseudo is required'
                },
                {
                    type:'string',
                    message : 'Please enter a valid pseudo'
                }
            ]}
            >
                <Input type = "pseudo" />
            </Form.Item>

        <Form.Item
            label="Password"
            name="password"
            rules={[
                {
                    required: true,
                    message: 'Password is required',
                },
            ]}
        >
            <Input.Password />
        </Form.Item>



        <Form.Item>
            <Button type="primary" htmlType="submit">
                Register
            </Button>
        </Form.Item>
    </Form>
    </>
    )
}