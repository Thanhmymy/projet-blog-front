import { Button, Card, Form, Input } from "antd";
import './Personal-form.css'



export function PersonalForm({ onFormSubmit, post = {} }) {


    function handleSubmit(values) {
        Object.assign(post, values)
        onFormSubmit(post)

    }

    return (
        <>
            <Card className="test">
                <h2>Exprime-toi</h2>
            <div className="personal-form">
                <Form onFinish={handleSubmit} initialValues={post}>
                    <Form.Item label="Post" name="texte">
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" >
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
            </Card>

        </>
    )
}