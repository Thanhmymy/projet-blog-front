import { Form, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { loginWithCredentials } from '../stores/auth-slice';

import './formulaire.css'

export function LoginForm({onSubmit}){

    
    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.loginBlog)

    const onFinish = (values) => {
        dispatch(loginWithCredentials(values));
    }
    
    return(
        
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 10,
            }}
            onFinish={onFinish}
        >
            {feedback && <p>{feedback}</p>}
            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {
                        required: true,
                        message: 'Email is required',
                    },
                    {
                        type: 'email',
                        message: 'Please enter a valid email',
                    },
                ]}
            >
                <Input type="email" />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Password is required',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>



            <Form.Item>
                <Button type="primary" htmlType="submit">
                    Login
                </Button>
            </Form.Item>
        </Form>
        

    )
}