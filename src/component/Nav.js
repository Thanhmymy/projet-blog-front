import { Menu } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { logout } from '../stores/auth-slice';
import './Nav.css'

export function Nav() {
    const location = useLocation();

    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user)
    return (
        <header>
            
            <Menu mode="horizontal" theme="dark" selectedKeys={[location.pathname]}>

                
                <Menu.Item key="/">
                    <Link to="/">Home
                    </Link>
                </Menu.Item>
                {!user ? <>
                    <Menu.Item key="/register">
                        <Link to="/register">Register</Link>
                    </Menu.Item>
                    <Menu.Item key="/login">
                        <Link to="/login">Login</Link>
                    </Menu.Item>
                </>
                    :
                    <>
                        <Menu.Item key="/personal">
                            <Link to="/personal">Personal</Link>
                        </Menu.Item>
                        <Menu.Item key="/logout" onClick={() => dispatch(logout())}>
                            Logout
                        </Menu.Item>
                    </>
                }

            </Menu>
            
        </header>

    )
}