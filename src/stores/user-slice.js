import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    list: []
};

const userSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        setList(state, {payload}) {
            state.list = payload;
        },
        addUser(state, {payload}) {
            state.list.push(payload);
        }
    }
});

export const {addUser,setList} = userSlice.actions;

export default userSlice.reducer;

export const fetchUsers = () => async (dispatch) => {
    try {
        const response = await axios.get('http://localhost:8000/api/user/all');
        dispatch(setList(response.data));
    } catch (error) {
        console.log(error);
    }
}
