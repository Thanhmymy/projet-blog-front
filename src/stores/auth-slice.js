import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";
import { addUser } from "./user-slice";


const authSlice = createSlice({

    name:'auth',
    initialState:{
        user : null,
        loginBlog : '',
        registerBlog:''
    },
    reducers :{
        login(state,{payload}){
            state.user = payload;
        },
        logout(state){
            state.user = null;
            localStorage.removeItem('token');
        },
        updateLoginBlog(state, {payload}){
            state.loginBlog = payload
        },
        updateRegisterBlog(state,{payload}){
            state.registerBlog= payload
        }
    }
});

export const {login, logout, updateLoginBlog, updateRegisterBlog} = authSlice.actions;

export default authSlice.reducer;

export const loginToken = () => async (dispatch) => {
    const token = localStorage.getItem('token');

    if (token){
        try {
            const user = await AuthService.fetchAccount();
            
            dispatch(login(user));
        } catch (error) {
            dispatch(logout());
        }
    } else{
        dispatch(logout());
    }

}

export const register = (body) => async (dispatch) => {
    try {
        const {user, token} = await AuthService.register(body);
        localStorage.setItem('token', token);

        dispatch(updateRegisterBlog('Register Successful'));
        dispatch(addUser(user));
        dispatch(loginWithCredentials(body))

    } catch (error) {
        dispatch(updateRegisterBlog(error.response.data.error));
    }
}

export const loginWithCredentials = (credentials) => async (dispatch) =>{
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginBlog('Login Sucessful'))
        dispatch(login(user));
    } catch (error) {
        dispatch(updateLoginBlog('Email or password incorrect'));
    }
}