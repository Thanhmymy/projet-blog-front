import './App.css';
import { Route, Switch} from 'react-router-dom';
import Home from './pages/Home';
import { OneArticle } from './pages/OneArticle';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { loginToken } from './stores/auth-slice';
import { Personal } from './pages/Personal';
import { Nav } from './component/Nav';

function App() {
  
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(loginToken());
  }, [dispatch]);
  
  return (
    <>
      <Nav/>
      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        <Route path='/article/one/:id'>
          <OneArticle/>
        </Route>

        <Route path="/register">
          <Register/>
        </Route>
        <Route path="/login">
            <Login />
          </Route>
          <Route path="/personal">
            <Personal />
          </Route>

      </Switch>
    </>
  );
}

export default App;
