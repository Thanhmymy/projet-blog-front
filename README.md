# Projet Blog

Ce projet est un projet individuel, de 2 semaines, pendant la formation Developpeur/se Web et web Mobile

## Langage utilisé

- React.js
- Node.js
- MySQL

## Librairie CSS utilisé

- Ant Design

## Ce qu'on a utilisé dans le Javascript

- Redux
- Rooter
- Dotenv-flow
- Express Server
- Token
- Cors

## Objectif

1. Faire la conception d'un blog, avec un back-end et un front-end en React.js. Avec une authentification.

## Organisation

Tout d'abord, j'ai commencé par faire une maquette, puis un UML, et un tableau Trello (Trello est l'outil de gestion visuelle du travail) qui m'a suivis tout le long du projet.

## Maquette : 
![alt text](src/Assets/maquette.png)

## UML :
![alt text](src/Assets/umlone.png)
![alt text](src/Assets/umltwo.png)

## Tableau Trello : 
![alt text](src/Assets/trello.png)


## Lien de mon projet

[Projet Blog](https://projet-blog.netlify.app)